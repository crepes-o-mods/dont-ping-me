package fr.creerio.dontpingme.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.message.v1.ClientReceiveMessageEvents;
import net.minecraft.client.MinecraftClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Environment(EnvType.CLIENT)
public class DontpingmeClient implements ClientModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("dontpingme");
    private static String PLAYER_NAME;

    @Override
    public void onInitializeClient() {
        PLAYER_NAME = MinecraftClient.getInstance().getSession().getUsername();

        ClientReceiveMessageEvents.GAME.register((message, overlay) -> {
            if (message.getString().contains(PLAYER_NAME)) {
                MinecraftClient.getInstance().close();
                throw new Error("Your name was mentioned, enjoy the crash !");
            }
        });
    }
}

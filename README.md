# Don't ping me

A ~~stupid~~ minecraft [Fabric](https://fabricmc.net/)/[Quilt](https://quiltmc.org/) mod which closes your game if your username is written in any way (either the server welcoming you or a player mentioning your name).

## Requirements/Dependencies

- [Fabric](https://fabricmc.net/) / [Quilt](https://quiltmc.org/)
- [Fabric API](https://modrinth.com/mod/fabric-api) / [QSL](https://modrinth.com/mod/qsl) (fabric-message-api-v1 is required more specifically, which should be bundled by either of these mods)

## FAQ

- Why?

Because reasons, and eggs. No but seriously, april 1st is in two days at the time of writing and I find the idea of "crashing" when having your name mentioned in any way both stupid and funny at the same time. You're free to troll your friends with this btw, just hope that the server won't mention your friend's name on join.

- Can you do X ?

No, but you are free to do whatever you want since this mod is in the public domain.